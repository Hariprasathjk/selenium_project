package Flipkart_Execution;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class flipkartPage {

	private WebDriver driver;

	public flipkartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//button[@class='_2KpZ6l _2doB4z']")
	private WebElement closePopupButton;

	@FindBy(xpath = "//input[@title='Search for products, brands and more']")
	private WebElement searchBox;

	@FindBy(xpath = "//button[@type='submit']")
	private WebElement searchButton;

	@FindBy(xpath = "(//select[@class='_2YxCDZ'])[1]")
	private WebElement minPriceDropdown;

	@FindBy(xpath = "//option[@value='15000']")
	private WebElement minPriceOption;

	@FindBy(xpath = "(//div[@class='_24_Dny'])[1]")
	private WebElement firstBrand;

	@FindBy(xpath = "(//div[@class='_4rR01T'])[1]")
	private WebElement ClickonPhone;
	
	@FindBy(xpath="//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button")
	private WebElement AddtoCart;
	
	@FindBy(xpath="//ul[@class='row']//button)[2]")
	private WebElement BuyNow;
	
	@FindBy(xpath="//input[@maxlength='auto']")
	private WebElement addtocartlogin;
	
	@FindBy(xpath="//button[@type='submit']")
	private WebElement submit;


	public void closePopup() {
		closePopupButton.click();
	}

	public void searchForProduct(String product) {
		searchBox.sendKeys(product);
		searchButton.click();
	}

	public void selectMinPriceRange() throws InterruptedException {
		Thread.sleep(8000);
		minPriceDropdown.click();
		minPriceOption.click();
	}

	public void selectBrand() throws InterruptedException {
		Thread.sleep(8000);
		firstBrand.click();
		Thread.sleep(5000);

		ClickonPhone.click();
	}
	
	public void addToCart() throws InterruptedException{
		driver.switchTo().frame(1);
		Thread.sleep(10000);
	AddtoCart.click();
    System.out.println("The product should be out of stock");
	driver.switchTo().defaultContent();
		}
	
	public void buyNow() throws InterruptedException {
		BuyNow.click();
		Thread.sleep(10000);
		addtocartlogin.sendKeys("8148592764");
		submit.click();
	}
	
	
}
