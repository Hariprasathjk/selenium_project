package Flipkart_Execution;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Test_Runner_Class {
	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		
		flipkartPage cart = new flipkartPage(driver);
		cart.closePopup();
		cart.searchForProduct("mobiles");
		cart.selectMinPriceRange();
		cart.selectBrand();
		cart.addToCart();
		cart.buyNow();
		
		driver.quit();
		
		
		
		
	}

}
